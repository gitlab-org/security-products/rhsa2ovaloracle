# frozen_string_literal: true

require 'bzip2/ffi'
require 'nokogiri'
require 'open-uri'

module Rhsa2OvalOracle
  class Oracle
    include SemanticLogger::Loggable

    ADVISORIES_URL = 'https://linux.oracle.com/security/oval/com.oracle.elsa-all.xml.bz2'

    attr_reader :temp_dir

    def initialize(temp_dir:)
      @temp_dir = temp_dir
    end

    def title_for(id)
      ids_and_title[id]
    end

    # Parse the oracle advisories to retrieve the oracle IDs and titles
    def ids_and_title
      @ids_and_title ||= begin
        advisories
        parse_advisories
      end
    end

    # Download the Oracle advisories in bzip2 format and extract the com.oracle.elsa-all.xml file to the temp directory
    def advisories
      logger.info('Fetching Oracle advisories')
      output_file = File.basename(ADVISORIES_URL, File.extname(ADVISORIES_URL))
      output_path = File.join(temp_dir, output_file)

      URI.parse(ADVISORIES_URL).open do |remote_file|
        uncompressed = Bzip2::FFI::Reader.read(remote_file)
        File.write(output_path, uncompressed)
      end
    end

    # Parse the com.oracle.elsa-all.xml file in the temp directory for Oracle advisories and return a hash with the
    # advisory ID as key and advisory title as value
    def parse_advisories
      logger.info('Parsing Oracle advisories')
      xml_doc = File.open(File.join(temp_dir, 'com.oracle.elsa-all.xml')) { |f| Nokogiri::XML(f) }
      definitions = xml_doc.xpath("//xmlns:definition")
      definitions.filter_map do |definition|
        match = definition['id'].match(/oval:com\.oracle\.elsa:def:(?<vulnerability_id>\d+)/)
        [match[:vulnerability_id], definition.at('title').text.strip] if match
      end.to_h
    end

    # This method does a few things:
    # 1. Set OS version for top level criterion
    # The Redhat advisory criteria does not include the OS version in the top level criterion. Set the OS version to
    # ensure that trivy-db parsing logic works https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db/-/blob/f498a19a/pkg/vulnsrc/oracle-oval/oracle-oval.go#L204-205
    # Example Redhat advisory top level criterion ../spec/fixtures/rhsa_2022_6778.json#92-97
    # Expected Oracle advisory top level criterion ../spec/expect/ELSA-2022-6778.json#259-263
    # 2. Rename OS
    # The "Red Hat Enterprise Linux" strings found in the criterions comment of Redhat advisories should be
    # renamed to "Oracle Linux". This is neccessary for trivy-db parsing logic to work https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db/-/blob/f498a19a/pkg/vulnsrc/oracle-oval/oracle-oval.go#L203
    def convert_criteria(redhat_criteria, os_version)
      oracle_criteria = {
        Operator: redhat_criteria['Operator'],
        Criterias: [],
        Criterions: []
      }

      # criteria can contain nested criterias. Recursively loop through them.
      redhat_criteria['Criterias']&.each_with_index do |sub_criteria, index|
        oracle_criteria[:Criterias][index] = convert_criteria(sub_criteria, os_version)
      end

      oracle_criteria[:Criterions] = redhat_criteria['Criterions']&.map do |sub_criterion|
        if sub_criterion['Comment'].include?('must be installed')
          # Set OS version for top level criterion
          { Comment: "Oracle Linux #{os_version} is installed" }
        else
          # Rename OS
          { Comment: sub_criterion['Comment'].gsub('Red Hat Enterprise Linux', 'Oracle Linux') }
        end
      end || []

      # this criteria doesn't exactly match the structure of the files in the trivy vuln-list repo
      # and instead relies on trivy implementation details which could cause problems if these
      # implementation details change.
      # TODO: replicate the same structure as files in the trivy vuln-list repo
      # see https://gitlab.com/gitlab-org/gitlab/-/issues/394806 for details.
      oracle_criteria
    end
  end
end
