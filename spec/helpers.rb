# frozen_string_literal: true

module Helpers
  def self.fixture_file(path)
    Pathname.new(__FILE__).parent.join('fixtures', path)
  end

  def self.fixture_file_content(path)
    fixture_file(path).read
  end

  def self.expect_file(path)
    Pathname.new(__FILE__).parent.join('expect', path)
  end

  def self.expect_file_content(path)
    expect_file(path).read
  end
end
