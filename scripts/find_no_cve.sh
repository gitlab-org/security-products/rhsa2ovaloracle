#!/bin/bash
# Find Oracle advisories that do not contain Cves
values=$(find ./temp/vuln-list/oval/oracle -type f -exec grep -l 'Cves": null' {} +)
result=""
while IFS= read -r line; do
    # Parse the Oracle file to retrieve the ELSA ID
    result=$(echo "$line" | xargs -I {} basename {} | cut -d'/' -f4)
    # Remove the ELSA- prefix
    result=${result#ELSA-}
    # Remove the .json filename
    result=${result%.json}
    # Format the ELSA ID(eg 2022-0001) in RHSA format(2022:0001)
    result="${result//-/:}"
    # Find Red Hat advisories that match the ELSA IDs with no cves
    find ./temp/vuln-list-redhat-main/oval -type f -name "*SA-$result*"
done <<< "$values"