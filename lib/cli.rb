#!/usr/bin/env ruby
# frozen_string_literal: true

require 'semantic_logger'
require 'tmpdir'
require 'optparse'

require_relative 'redhat'
require_relative 'oracle'

SemanticLogger.default_level = ENV.fetch('SECURE_LOG_LEVEL', :info)
SemanticLogger.add_appender(io: $stdout)

module Rhsa2OvalOracle
  class CLI
    include SemanticLogger::Loggable

    attr_reader :temp_dir, :redhat

    def self.execute!(options: nil)
      new(options: options).run
    end

    def initialize(options: nil)
      @temp_dir = Dir.mktmpdir
      @redhat = Redhat.new(temp_dir: temp_dir, options: options)

      logger.info("Saving temporary files to #{temp_dir}")
    end

    # Fetch and convert Redhat advisories into Oracle advisory format
    def run
      redhat.fetch_and_convert_advisories

      temp_dir
    end
  end
end
