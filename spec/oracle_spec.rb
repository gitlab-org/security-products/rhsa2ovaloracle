# frozen_string_literal: true

require 'semantic_logger'
require 'spec_helper'
require 'fakefs/spec_helpers'

require 'tmpdir'

require_relative '../lib/oracle'

RSpec.describe Rhsa2OvalOracle::Oracle do
  let(:temp_dir) { Dir.mktmpdir }
  subject { described_class.new(temp_dir: temp_dir) }

  let(:compressed_oracle_advisories) { Helpers.fixture_file_content('com.oracle.elsa-all.xml.bz2') }

  before do
    stub_request(:get, Rhsa2OvalOracle::Oracle::ADVISORIES_URL).to_return(
      status: 200, body: compressed_oracle_advisories
    )
  end

  describe '#advisories' do
    include FakeFS::SpecHelpers

    it 'downloads and decompresses the oval-oracle file' do
      subject.advisories
      file_content = File.read(File.join(subject.temp_dir, 'com.oracle.elsa-all.xml'))
      expect(file_content).to match('<definition id="oval:com.oracle.elsa:def:20235689">')
    end
  end

  # Track sample oracle definition and return correctly parsed IDs
  describe '#parse_advisories' do
    let(:temp_dir) { 'spec/fixtures' }

    it 'returns list of matching oracle IDs' do
      # parse_oracle_advisories parses the fixture file com.oracle.elsa-all.xml which contains the
      # expected oracle advisories
      expect(subject.parse_advisories).to eq({
        '19981001' => 'ELSA-1998-1001: advisory title',
        '19981002' => 'ELSA-1998-1002: advisory title',
        '19981003' => 'ELSA-1998-1003: advisory title',
        '20110542' => 'ELSA-2011-0542:  Oracle Linux 6.1 kernel security, bug fix and enhancement update (IMPORTANT)',
        '20235689' => 'ELSA-2023-5689:  bind security update (IMPORTANT)'
      })
    end
  end

  describe '#convert_criteria' do
  let(:redhat_advisory) { JSON.parse(Helpers.fixture_file_content('rhsa_2022_6778.json')) }
    let(:expected_advisory) do
      JSON.parse(Helpers.expect_file_content('ELSA-2022-6778.json'), symbolize_names: true)
    end

    it 'converts the redhat criteria to oracle format' do
      expect(subject.convert_criteria(redhat_advisory['Criteria'], '8')).to eq(expected_advisory[:Criteria])
    end
  end
end
