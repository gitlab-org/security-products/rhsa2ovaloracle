#!/usr/bin/env ruby
# frozen_string_literal: true

require 'semantic_logger'
require 'optparse'
require 'hashie'

require_relative 'cli'

module Rhsa2OvalOracle
  class Main
    def self.parse(options)
      args = Hashie::Mash.new

      opt_parser = OptionParser.new do |opts|
        opts.banner = 'Usage: main.rb [options]'

        opts.on(
          '-oDIR', '--output-dir=DIR', "Directory to save files to. (default: temporary dir)"
        ) do |output_dir|
          args.output_dir = output_dir
        end

        opts.on('-h', '--help', 'Show help') do
          puts opts
          exit
        end
      end

      opt_parser.parse!(options)
      args
    end

    def self.execute!
      options = parse(ARGV)
      Rhsa2OvalOracle::CLI.execute!(options: options)
    end
  end
end

Rhsa2OvalOracle::Main.execute!
