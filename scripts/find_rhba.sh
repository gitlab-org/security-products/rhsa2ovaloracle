#!/bin/bash
# Find Red Hat advisories that have prefix RHBA
values=$(find ./temp/vuln-list-redhat-main/oval -type f -exec grep -l 'Title": "RHBA' {} +)
result=""
while IFS= read -r line; do
    # Parse the Red Hat file to retrieve the RHBA ID
    result=$(echo "$line" | xargs -I {} basename {} | cut -d'/' -f4)
    # Remove the RHBA- prefix
    result=${result#RHBA-}
    # Remove the .json filename
    result=${result%.json}
    # Format the RHBA ID(eg 2022:0001) in ELSA format(2022-0001)
    result="${result//:/-}"
    # Find Oracle advisories that match the RHBA IDs
    find ./temp/vuln-list/oval/oracle -type f -name "ELSA-$result.json"
done <<< "$values"