#!/bin/bash
# Find Red Hat advisories that have prefix RHEA
values=$(find ./temp/vuln-list-redhat-main/oval -type f -exec grep -l 'Title": "RHEA' {} +)
result=""
while IFS= read -r line; do
    # Parse the Red Hat file to retrieve the RHEA ID
    result=$(echo "$line" | xargs -I {} basename {} | cut -d'/' -f4)
    # Remove the RHEA- prefix
    result=${result#RHEA-}
    # Remove the .json filename
    result=${result%.json}
    # Format the RHEA ID(eg 2022:0001) in ELSA format(2022-0001)
    result="${result//:/-}"
    # Find Oracle advisories that match the RHEA IDs
    find ./temp/vuln-list/oval/oracle -type f -name "ELSA-$result.json"
done <<< "$values"