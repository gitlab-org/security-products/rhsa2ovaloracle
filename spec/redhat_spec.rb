# frozen_string_literal: true

require 'semantic_logger'
require 'spec_helper'
require 'fakefs/spec_helpers'
require 'rspec/json_expectations'
require 'rspec-parameterized'

require_relative '../lib/redhat'
require_relative '../lib/oracle'

RSpec.describe Rhsa2OvalOracle::Redhat do
  subject { described_class.new(temp_dir: temp_dir) }

  let(:temp_dir) { 'some-temp-dir' }
  let(:compressed_redhat_advisories) { Helpers.fixture_file_content('vuln-list-redhat-main.tgz') }
  let(:oracle) { instance_double(Rhsa2OvalOracle::Oracle, :oracle) }

  before do
    stub_request(:get, Rhsa2OvalOracle::Redhat::VULN_LIST_URL).to_return(
      status: 200, body: compressed_redhat_advisories
    )

    allow(Rhsa2OvalOracle::Oracle).to receive(:new).and_return(oracle)
    allow(oracle).to receive(:title_for).and_return("some title")
  end

  describe '#fetch_advisories' do
    include FakeFS::SpecHelpers

    it 'downloads and extracts the vuln-list-redhat repository' do
      subject.fetch_advisories
      expected_file_path = File.join(subject.temp_dir,
        'vuln-list-redhat-main/oval/9/rhel-9/definitions/2023/RHSA-2023:5689.json')
      expect(File.read(expected_file_path)).to include('"Title": "RHSA-2023:5689: bind security update (Important)"')
    end
  end

  describe '#advisories' do
    include FakeFS::SpecHelpers

    RSpec.shared_examples 'expected redhat advisories' do
      before do
        advisory_files.each do |file|
          FileUtils.mkdir_p(File.join(subject.temp_dir, File.dirname(file)))
          File.write(File.join(subject.temp_dir, file), '{}')
        end
      end

      it 'returns all the redhat advisories' do
        expect(subject.advisories).to match(advisories)
      end
    end

    context 'when directory contains multiple redhat advisory and non-advisory files' do
      let(:advisory_files) do
        [
          "vuln-list-redhat-main/oval/9/openshift-4.12/definitions/2023/RHSA-2023:0727.json",
          "vuln-list-redhat-main/oval/5/jboss-eap-6/definitions/2012/RHSA-2012:1591.json",
          "vuln-list-redhat-main/oval/9/openshift-4.12/states/states.json",
          "vuln-list-redhat-main/oval/5/jboss-eap-6/objects/objects.json"
        ]
      end

      let(:advisories) do
        {
          "20230727" => "/#{subject.temp_dir}/vuln-list-redhat-main/oval/9/openshift-4.12/definitions/2023/RHSA-2023:0727.json",
          "20121591" => "/#{subject.temp_dir}/vuln-list-redhat-main/oval/5/jboss-eap-6/definitions/2012/RHSA-2012:1591.json"
        }
      end

      it_behaves_like 'expected redhat advisories'
    end

    context 'when there are multiple Red Hat advisories with the same ID' do
      let(:advisory_files) do
        [
          "vuln-list-redhat-main/oval/9/openshift-4.12/definitions/2023/RHSA-2023:0727.json",
          "vuln-list-redhat-main/oval/5/jboss-eap-6/definitions/2023/RHSA-2023:0727.json"
        ]
      end

      let(:advisories) do
        {
          "20230727" => "/#{subject.temp_dir}/vuln-list-redhat-main/oval/9/openshift-4.12/definitions/2023/RHSA-2023:0727.json"
        }
      end

      it_behaves_like 'expected redhat advisories'
    end

    context 'when Red Hat advisory ID does not match an Oracle ID' do
      let(:advisory_files) { ["vuln-list-redhat-main/oval/5/jboss-eap-6/definitions/2012/RHSA-2012:1591.json"] }
      let(:advisories) { {} }

      before do
        allow(oracle).to receive(:title_for).and_return(false)
      end

      it_behaves_like 'expected redhat advisories'
    end
  end

  describe '#convert_advisories' do
    include FakeFS::SpecHelpers

    let(:advisories) do
      {
        "20226778" => "/temp/vuln-list-redhat-main/oval/5/jboss-eap-6/definitions/2022/RHSA-2022:6778.json"
      }
    end

    it 'converts the advisory and saves file' do
      allow(subject).to receive(:advisories).and_return(advisories)
      expect(File).to receive(:read).with(advisories.values.first).and_return('{}')

      expect(subject).to receive(:convert_advisory).once.and_return(
        { advisory_id: '1234', advisory: 'advisory data'})
      expect(subject).to receive(:save_to_file).with('1234', 'advisory data', '2022').once

      subject.convert_advisories
    end
  end

  describe '#new_advisory' do
    before do
      allow(oracle).to receive(:convert_criteria).and_return(expected_advisory[:Criteria])
    end
    
    let(:redhat_advisory) { JSON.parse(Helpers.fixture_file_content('rhsa_2022_6778.json')) }
    let(:expected_advisory) do
      advisory = JSON.parse(Helpers.expect_file_content('ELSA-2022-6778.json'), symbolize_names: true)
      advisory[:References] = []
      advisory[:Cves] = []
      advisory
    end
    
    it 'creates a new advisory with empty reference and cves' do
      expect(subject.new_advisory(redhat_advisory['Criteria'], redhat_advisory['Metadata'])).to eq(expected_advisory)
    end
  end

  describe '#add_reference_to' do
    let(:advisory) {{}}
    let(:redhat_advisory) {JSON.parse(Helpers.fixture_file_content('rhsa_2022_6778.json'))}
    let(:expected_advisory) do
      JSON.parse(Helpers.expect_file_content('ELSA-2022-6778.json'), symbolize_names: true)
    end

    it 'converts the advisory references to oracle format' do
      expect(subject.add_reference_to(advisory, redhat_advisory['Metadata'])).to eq(["RHSA-2022:6778", "ELSA-2022-6778"])
      expect(advisory[:References]).to eq(expected_advisory[:References])
    end
  end

  describe '#convert_advisory' do
    before do
      allow(oracle).to receive(:convert_criteria).and_return(expected_advisory[:Criteria])
    end

    context 'when advisory is convertable' do
      let(:redhat_advisory) { JSON.parse(Helpers.fixture_file_content('rhsa_2022_6778.json')) }
      let(:expected_advisory) do
        JSON.parse(Helpers.expect_file_content('ELSA-2022-6778.json'), symbolize_names: true)
      end

      let(:expected_advisory_id) { 'ELSA-2022-6778' }

      it 'converts the advisory to the oval format' do
        expect(subject.convert_advisory(redhat_advisory, expected_advisory[:Title]))
          .to eq(advisory_id: expected_advisory_id, advisory: expected_advisory)
      end
    end

    context 'when advisory platform is not convertable' do
      let(:redhat_advisory) { JSON.parse(Helpers.fixture_file_content('rhsa_2012_1591_not_convertable_platform.json')) }
      let(:expected_advisory) { {} }

      it 'returns an empty hash' do
        expect(subject.convert_advisory(redhat_advisory, "")).to be_nil
      end
    end

    context 'when Red Hat and Oracle advisory title does not match' do
      let(:redhat_advisory) { JSON.parse(Helpers.fixture_file_content('rhsa_2022_6778.json')) }
      let(:expected_advisory) { {} }

      it 'returns an empty hash' do
        expect(subject.convert_advisory(redhat_advisory, "unmatched title")).to be_nil
      end
    end
  end

  describe '#save_to_file' do
    include FakeFS::SpecHelpers

    let(:advisory_id) { 'RHSA-2022-6778' }
    let(:year) { 2022 }

    it 'writes the advisory to a file' do
      advisory = { hello: 'world' }

      subject.save_to_file(advisory_id, advisory, year)

      target_file = File.join(subject.temp_dir, 'oval', 'oracle', year.to_s, "#{advisory_id}.json")

      expect(File).to exist(target_file)
      expect(JSON.parse(File.read(target_file))).to include_json(advisory)
    end
  end

  describe '#titles_match' do
    using RSpec::Parameterized::TableSyntax

    # rubocop:disable Layout/LineLength
    where(:test_case_name, :redhat_id, :oracle_id, :redhat_title, :oracle_title, :expectation) do
      'IDs differ'         | "RHSA-2022:6778" | "ELSA-2022-6778" | "RHSA-2022:6778: security update" | "ELSA-2022-6778: security update"      | true
      'IDs/spacing differ' | "RHSA-2022:6778" | "ELSA-2022-6778" | "RHSA-2022:6778: security update" | "ELSA-2022-6778:      security update" | true
      'IDs/case differ'    | "RHSA-2022:6778" | "ELSA-2022-6778" | "RHSA-2022:6778: SECURITY UPDATE" | "ELSA-2022-6778: security update"      | true
      'name differs'       | "RHSA-2022:6778" | "ELSA-2022-6778" | "RHSA-2022:6778: security update" | "ELSA-2022-6778: kernel update"        | false
      'name differs by os' | "RHSA-2011:0542" | "ELSA-2011-0542" | "RHSA-2011:0542: Red Hat Enterprise Linux 6.1 kernel update (Important)"  | "ELSA-2011-0542:  Oracle Linux 6.1 kernel update (IMPORTANT)" | true
    end
    # rubocop:enable Layout/LineLength

    with_them do
      it 'returns the expected value' do
        expect(subject.titles_match(redhat_id, oracle_id, redhat_title, oracle_title)).to eq(expectation)
      end
    end
  end
end
