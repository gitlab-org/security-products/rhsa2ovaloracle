# frozen_string_literal: true

require 'minitar'
require 'open-uri'
require 'json'
require 'fileutils'

module Rhsa2OvalOracle
  class Redhat
    include SemanticLogger::Loggable

    VULN_LIST_URL = 'https://github.com/aquasecurity/vuln-list-redhat/archive/main.tar.gz'

    attr_reader :temp_dir, :oracle, :output_dir

    def initialize(temp_dir:, options: nil)
      @temp_dir = temp_dir
      @oracle = Oracle.new(temp_dir: temp_dir)
      @output_dir = File.join(options&.output_dir || temp_dir, 'oval/oracle')

      logger.info("Saving converted files to #{output_dir}")
    end

    # Fetch and convert Redhat advisories into Oracle advisory format
    def fetch_and_convert_advisories
      fetch_advisories
      convert_advisories
    end

    # Download the vuln-list-redhat repository in gz format and extract the files to the temp directory
    def fetch_advisories
      logger.info('Fetching Redhat advisories')

      URI.parse(VULN_LIST_URL).open do |remote_file|
        gz_reader = Zlib::GzipReader.new(remote_file)
        Minitar.unpack(gz_reader, temp_dir)
      end
    end

    # This method does a few things:
    # 1. Search for Redhat advisory files in temp/vuln-list-redhat-main/oval directory since there are non advisory
    # files nested in this directory
    # 2. Filter Redhat advisories that have the same ID
    # 3. Check if the Redhat advisory ID matches with an Oracle advisory ID
    # It then returns these filtered Redhat advisories in a hash with key as ID and value as the file_path
    def advisories
      logger.info('Filtering Redhat advisories')

      redhat_files = Dir[File.join(temp_dir, 'vuln-list-redhat-main/oval/**/RHSA-*.json')]

      redhat_files.filter_map do |file_path|
        # Convert Redhat ID to Oracle ID format
        match = File.basename(file_path).match(/RHSA-(?<id>\d+:\d+)/)
        oracle_id = match[:id].delete(':')
        # Only add advisory if it matches an Oracle advisory ID
        [oracle_id, file_path] if oracle.title_for(oracle_id)
      end.to_h
    end

    # Convert Redhat advisories to Oracle advisory format and save file
    def convert_advisories
      logger.info('Converting Redhat advisories')

      advisories.each do |redhat_id, file_path|
        year = redhat_id[0..3]
        oracle_title = oracle.title_for(redhat_id)
        redhat_advisory = JSON.parse(File.read(file_path))
        result = convert_advisory(redhat_advisory, oracle_title)
        save_to_file(result[:advisory_id], result[:advisory], year) if result
      end
    end

    # Save advisory to oval/oracle directory
    def save_to_file(advisory_id, advisory, year)
      target_file = File.join(output_dir, year.to_s, "#{advisory_id}.json")
      FileUtils.mkdir_p(File.dirname(target_file))
      logger.debug("Writing #{target_file}")
      File.write(target_file, JSON.pretty_generate(advisory))
    end

    # Create a new advisory based on Oracle advisory format using the redhat_advisory data
    def new_advisory(redhat_criteria, redhat_metadata)
      match = redhat_metadata['AffectedList'][0]['Platforms'][0].match(/Red Hat Enterprise Linux (?<os>[0-9]+)/)
      return unless match

      os_version = match[:os]

      # Parse response in Oracle format that Trivy requires.
      # Example format: https://gitlab.com/gitlab-org/secure/vulnerability-research/advisories/vuln-list-mirror/-/blob/main/oval/oracle/2023/ELSA-2023-0005.json
      {
        Title: redhat_metadata['Title'].sub(/^[A-Z]{4}/, 'ELSA').sub(':', '-'),
        Description: redhat_metadata['Description'],
        Platform: ["Oracle Linux #{os_version}"],
        References: [],
        Criteria: oracle.convert_criteria(redhat_criteria, os_version),
        Severity: redhat_metadata['Advisory']['Severity'].upcase,
        Cves: [],
        Issued: {
          Date: redhat_metadata['Advisory']['Issued']['Date']
        }
      }
    end

    # Modify Redhat references to Oracle reference format
    def add_reference_to(advisory, redhat_metadata)
      redhat_id = ""
      oracle_id = ""
      advisory[:References] = redhat_metadata['References']&.map do |reference|
        if reference['Source'] == 'RHSA'
          redhat_id = reference['RefID']
          oracle_id = redhat_id.tr(':', '-').sub(/^[A-Z]{4}/, 'ELSA')
          oracle_uri = "https://linux.oracle.com/errata/#{oracle_id}.html"
          oracle_source = 'elsa'
          { Source: oracle_source, URI: oracle_uri, ID: oracle_id }
        else
          { Source: reference['Source'], URI: reference['RefURL'], ID: reference['RefID'] }
        end
      end || []

      [redhat_id, oracle_id]
    end

    # This method does a few things:
    # 1. Convert Redhat adivsory to Oracle advisory format
    # 2. Check that advisory titles match
    # We discovered instances where Redhat and Oracle advisory IDs match but the titles differ. This could result in the
    # wrong package and version being flagged as vulnerable in Oracle when it only applies to Redhat.
    # As such, it is essential to ensure that the titles match before including the advisory. More details in https://gitlab.com/gitlab-org/gitlab/-/issues/393478#note_1578740605
    def convert_advisory(redhat_advisory, oracle_title)
      # Convert Redhat adivsory to Oracle advisory format
      redhat_metadata = redhat_advisory['Metadata']
      redhat_title = redhat_metadata['Title']

      advisory = new_advisory(redhat_advisory['Criteria'], redhat_metadata)
      unless advisory
        logger.debug("Skipping advisory as platform is unparsable #{redhat_title}")
        return
      end

      redhat_id, oracle_id = add_reference_to(advisory, redhat_metadata)

      # Check that advisory titles match
      unless titles_match(redhat_id, oracle_id, redhat_title, oracle_title)
        logger.debug(
          %(Skipping advisory because titles don't match. Expected '#{redhat_title}' to match '#{oracle_title}'))
        return
      end

      advisory[:Cves] = redhat_metadata.dig('Advisory', 'Cves')&.map do |cve|
        { Impact: '', Href: "https://linux.oracle.com/cve/#{cve['CveID']}.html", ID: cve['CveID'] }
      end || []

      { advisory: advisory, advisory_id: oracle_id }
    end

    # Redhat advisory titles might differ slightly and this method does some basic parsing to compare with
    # the Oracle advisory title. See spec for detailed scenarios
    def titles_match(redhat_id, oracle_id, redhat_title, oracle_title)
      parsed_redhat_title = redhat_title.gsub("#{redhat_id}:", "").downcase.gsub('red hat enterprise linux', '').strip
      parsed_oracle_title = oracle_title.gsub("#{oracle_id}:", "").downcase.gsub('oracle linux', '').strip

      parsed_redhat_title == parsed_oracle_title
    end
  end
end
