# frozen_string_literal: true

require_relative '../lib/cli'
require_relative '../lib/redhat'
require_relative '../lib/oracle'

require 'semantic_logger'
require 'fakefs/spec_helpers'
require 'rspec/json_expectations'
require 'rspec-parameterized'
require 'spec_helper'

RSpec.describe Rhsa2OvalOracle::CLI do
  let(:compressed_redhat_advisories) { Helpers.fixture_file_content('vuln-list-redhat-main.tgz') }
  let(:compressed_oracle_advisories) { Helpers.fixture_file_content('com.oracle.elsa-all.xml.bz2') }

  before do
    stub_request(:get, Rhsa2OvalOracle::Redhat::VULN_LIST_URL).to_return(
      status: 200, body: compressed_redhat_advisories
    )

    stub_request(:get, Rhsa2OvalOracle::Oracle::ADVISORIES_URL).to_return(
      status: 200, body: compressed_oracle_advisories
    )
  end

  describe '.execute!' do
    subject { described_class.execute! }

    let!(:expected_file_contents) { Helpers.expect_file_content('oval/oracle/2023/ELSA-2023-5689.json') }

    it 'creates the expected files' do
      FakeFS do
        temp_dir = subject
        actual_file_contents = File.read(File.join(temp_dir, 'oval/oracle/2023/ELSA-2023-5689.json'))
        expect(actual_file_contents).to eq(expected_file_contents)
      end
    end
  end

  describe '#run' do
    include FakeFS::SpecHelpers

    subject { described_class.new }

    it 'invokes functions' do
      expect(subject.redhat).to receive(:convert_advisories).once
      subject.run
    end
  end
end
