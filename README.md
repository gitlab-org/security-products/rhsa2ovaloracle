# rhsa2ovaloracle
This tool converts [vuln-list-redhat](https://github.com/aquasecurity/vuln-list-redhat/tree/c3480e84c3a9da51836ddc28238f3a04c7b82a13/oval) `Red Hat advisories` into [oracle advisory json files](https://gitlab.com/gitlab-org/secure/vulnerability-research/advisories/vuln-list-mirror/-/tree/37dc2e1803fef239b63d1b705ccb80f37462afe8/oval/oracle) which is used by [trivy-db-glad](https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad) to generate the vulnerability db used by [container-scanning](https://gitlab.com/gitlab-org/security-products/analyzers/container-scanning).

## How it works
1. In this repository a daily scheduled pipeline will fetch and convert all advisories found in [vuln-list-redhat](https://github.com/aquasecurity/vuln-list-redhat/tree/c3480e84c3a9da51836ddc28238f3a04c7b82a13/oval) into `oracle advisory json files` like the ones found [here](https://gitlab.com/gitlab-org/secure/vulnerability-research/advisories/vuln-list-mirror/-/tree/37dc2e1803fef239b63d1b705ccb80f37462afe8/oval/oracle).
    - There are multiple `Red Hat advisories` of the same ID in `vuln-list-redhat` and only 1 instance is converted.
    - As not all `Red Hat advisories` are relevant to `Oracle advisories`, the script also checks that the ID of the RHSA advisory matches against the oracle advisory before conversion.
    - The tool uses [oras](https://oras.land/) to push the converted `oracle advisory json files` to this project's registry.
2. [trivy-db-glad repo](https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad) has a daily scheduled pipeline that [pulls the oracle advisory json files](https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad/-/blob/02927fa34a72882683b7603a78d4be18b82cd600/.gitlab-ci.yml#L68) to generate the vulnerability database [trivy.db](https://gitlab.com/gitlab-org/security-products/dependencies/trivy-db-glad/-/blob/02927fa34a72882683b7603a78d4be18b82cd600/.gitlab-ci.yml#L75)
3. [container-scanning repo]() has a daily scheduled pipeline that builds an image with the latest `trivy.db` for use in container scanning.

## Run locally
Install `bundle`
Run `SECURE_LOG_LEVEL=debug bundle exec bin/rhsa2ovaloracle`.
Converted files can be found in `oval/` directory

## Run in pipeline
Run pipeline and manually populate the ENV variable `ADVISORY_GEN=true`

## Schedule
This pipeline is scheduled to run daily to fetch new advisories with the ENV variable `ADVISORY_GEN=true`

## Scripts
1. `find_no_cve.sh`
    - In a [previous version](https://gitlab.com/gitlab-org/security-products/rhsa2ovaloracle/-/tree/0a087e141638a93e720c6200c7d4adfb84299cbe/) of this script, `Red Hat advisories` that did not contain cve entries were converted. For the new iteration, this script was used to check if there were `Red Hat advisories` that matches `Oracle advisories` that do not contain cves. 
    - No `Red Hat advisories` were found and as such, [this test case](https://gitlab.com/gitlab-org/security-products/rhsa2ovaloracle/-/blob/main/spec/lib/rhsa2ovaloracle/cli_spec.rb#L47-65) has been excluded from the implementation
    - To run it, first ensure that you have ran the [Run locally](#Run-locally) section to create a `./temp` directory. This is used to reference the `vuln-list-redhat` data
    - Run script
        ```sh
            cd ../temp
            git clone https://github.com/aquasecurity/vuln-list.git
            cd ../
            chmod +x scripts/find_no_cve.sh
            ./scripts/find_no_cve.sh
        ```

2. `find_rhba.sh`
    - In a [previous version](https://gitlab.com/gitlab-org/security-products/rhsa2ovaloracle/-/tree/0a087e141638a93e720c6200c7d4adfb84299cbe/) of this script, `Red Hat advisories` that has prefix `RHBA` were converted. For the new iteration, this script was used to check if there were `Red Hat advisories` with `RHBA` prefix that matches `Oracle advisories`.
    - No `RHBA Red Hat advisories` were found to match with `Oracle advisories`, as such `RHBA Red Hat advisories` have been excluded from the code

3. `find_rhea.sh`
    - In a [previous version](https://gitlab.com/gitlab-org/security-products/rhsa2ovaloracle/-/tree/0a087e141638a93e720c6200c7d4adfb84299cbe/) of this script, `Red Hat advisories` that has prefix `RHEA` were converted. For the new iteration, this script was used to check if there were `Red Hat advisories` with `RHEA` prefix that matches `Oracle advisories`.
    - No `RHEA Red Hat advisories` were found to match with `Oracle advisories`, as such `RHEA Red Hat advisories` have been excluded from the code